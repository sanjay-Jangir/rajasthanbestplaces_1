<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'rajasthanbestplaces_1' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '{-$cp36-m*{xgx;(vCGCWCl13 1E8rH?Gg.Hp+{i?qg.!E0P%X]QX$i^lNw,xHJ2' );
define( 'SECURE_AUTH_KEY',  'puF$S)6>/4 yIAnXK7Wi05 +x<2;00aW[TV*siy3c1#}y:LW_nWRymDf7 tX%65t' );
define( 'LOGGED_IN_KEY',    'OKN5WM$Mi*Cq`~E[f+$>z,#i)lkS-v*`f!Pk+fz0=|@`nG[zZw-^_SmtJoF{+ogm' );
define( 'NONCE_KEY',        '#;8&P:HY4NV^{vWOPAf,x;q!uW_~K5,0fMa1vv0-w3^>SNy.~c{-ZhLviX37c*bt' );
define( 'AUTH_SALT',        'pI2rS5rm7j04!wU&*V7VJ8$<lOlyQ0+z z[wcOfHD{LU0|a^TwCV35HwbJ}GnE64' );
define( 'SECURE_AUTH_SALT', 'Of1 sKYj$6:XMr;Rq#1g+xUiKl%1bVb/QS6w?TEwTK-i,ezZt!v(TfHw45l`o>Qc' );
define( 'LOGGED_IN_SALT',   's&1^m,y$tn1M;?Z)]W)R9N7U-(:72.k:x7! E4ms8cVq!4jLNE|9dT:F-qvh:w/K' );
define( 'NONCE_SALT',       '^7tCv7X4q3dG)@^ah=PMhesAjO&QW^-)wd2NtIxDT7WmyT`0M<;E,#=!Z|0Z|S.s' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'bbgo_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
